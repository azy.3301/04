package Program2;

/**
 * Class Hairdresser implements interface Runnable 
 * @author AZY
 * */
public class Hairdresser implements Runnable {

    /** minimum delay */
    private static final int LATENCY = 2000;

    /** maximum delay */
    private static final int LATENCY_BOUNDARY = 4000;

    /** Hairdresser status */
    public HairdresserStatus status;

    /** Reception */
    public Reception reception;

    /** Haircut chair */
    public HaircutChair haircutChair;

    /**
     * Constructor Hairdresser
     * @param reception reception 
     * @param haircutChair the chair on which the clients of this hairdresser sit
     */
    Hairdresser(Reception reception, HaircutChair haircutChair) {
        this.reception = reception;
        this.haircutChair = haircutChair;
    }

    /**
     * We wake up the hairdresser
     * @param client - client
     */
    public synchronized void wakeUp(Client client) {
        status = HairdresserStatus.WORK;
        haircut(client);
        this.notify();
    }

    /**
     * Hairdresser cutting hair
     * @param client - client
     */
    private void haircut(Client client) {
        try {
            System.out.println("Hairdresser cutting hair " + client.name);
            haircutChair.takeSeat(client);
            Thread.sleep(LATENCY + (int) (Math.random() * LATENCY_BOUNDARY));
            System.out.println("The hairdresser finished the haircut " + client.name);
            haircutChair.free();
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }

    /** Starts thread */
    public synchronized void run() {
        while (true) {
            System.out.println("The hairdresser comes to the chairs and looks");
            if (reception.сlientsOnChairs.size() > 0) {
                status = HairdresserStatus.WORK;
                haircut(reception.getClient());
            } else {
                System.out.println("There are no clients, the hairdresser went to bed");
                status = HairdresserStatus.SLEEP;
                try {
                    this.wait();
                } catch (InterruptedException e) {}
            }
        }
    }
}
