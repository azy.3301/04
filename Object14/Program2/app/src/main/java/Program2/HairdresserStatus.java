package Program2;

/**
 * Barber statuses
 * @author AZY
 */
public enum HairdresserStatus {
    SLEEP, WORK
}
