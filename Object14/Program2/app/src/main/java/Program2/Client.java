package Program2;

/**
 * Class Client implements interface Runnable
 * @author AZY
 */
public class Client implements Runnable {

    /** Hairdresser */
    private Hairdresser hairdresser;
    
    /** Reception */
    private Reception reception;

    /** Сlient name */
    public String name;

    /**
     * Сonstructor Client
     * @param name clienе name
     * @param hairdresser hairdresser
     * @param reception reception
     */
    Client(String name, Hairdresser hairdresser, Reception reception) {
        this.name = name;
        this.hairdresser = hairdresser;
        this.reception = reception;
    }

    /** Starts thread */
    public void run() {
        if (hairdresser.status.equals(HairdresserStatus.SLEEP)) {
            System.out.println(this.name + " woke up the hairdresser"); 
            hairdresser.wakeUp(this);
        } else {
            System.out.println(this.name + " goes to the reception");
            if (reception.sitChair(this)) {
                System.out.println(this.name + " sits on a chair and waits");
            } else {
                System.out.println(this.name + " gone, no free chair");
            }
        }
    }
}
