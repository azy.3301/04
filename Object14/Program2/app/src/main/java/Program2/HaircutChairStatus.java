package Program2;

/**
 * Statuses that a barber chair can accept
 * @author AZY
 */
public enum HaircutChairStatus {
    BUSY, FREE
}
