package Program2;

import java.util.concurrent.ArrayBlockingQueue;

/**
 * Class Reception 
 * @author AZY
 */
public class Reception {
    
    /** Number of chairs */
    private static final int MAX_CHAIRS_IN_RECEPTION = 5;
    
    /** Client Queue */
    ArrayBlockingQueue<Client> сlientsOnChairs = new ArrayBlockingQueue<>(MAX_CHAIRS_IN_RECEPTION);
    
    /**
     * Put the client in the queue
     * @return boolean
     */
    public synchronized boolean sitChair(Client client) {
        return сlientsOnChairs.offer(client);
    }

    /**
     * Returns the first client from the queue.
     * @return Client
     */
    public synchronized Client getClient() {
        return сlientsOnChairs.poll();
    }
}
