package Program2;

/**
 * Haircut chair
 * @author AZY
 */
public class HaircutChair {
    
    /** Chair status */
    public HaircutChairStatus status = HaircutChairStatus.FREE;
    
    /** 
     * We put the client on a chair to the hairdresser
     * @param client this new client
     */
    public synchronized void takeSeat(Client client) {
        if (status.equals(HaircutChairStatus.BUSY)) {
            try {
                wait();
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }
        System.out.println(client.name + " sitting in a chair");
        status = HaircutChairStatus.BUSY;
    }
    
    /** We release the chair */
    public synchronized void free() {
        System.out.println("The chair is free");
        status = HaircutChairStatus.FREE;
        notifyAll();
    }
}
