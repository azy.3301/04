package Program1;

class MyThreadWrite extends Thread {

    static final int LATENCY_START = 1000;
    
    static final int LATENCY_FINISH = 4000;

    @Override
    public void run(){
        while (true){
            System.out.println("Write " + (++Book.i) + "\n");
            try {
                Thread.sleep(LATENCY_START + (int) (Math.random() * LATENCY_FINISH));
            } catch (InterruptedException e){
                e.printStackTrace();
            }
        }
    }
}