package Program3;

import java.util.ArrayList;

/**
 * Class of the bartender who serves smokers
 */
public class Table {
    
    /** are there any matches */
    private boolean hasMatches;
    
    /** are there any Tobacco */
    private boolean hasTobacco;
    
    /** are there any Paper */
    private boolean hasPaper;

    /**
     * getting the matches
     * @return boolean
     */
    public boolean hasMatches() {
        return hasMatches;
    }

    /**
     * getting the Tobacco
     * @return boolean
     */
    public boolean hasTobacco() {
        return hasTobacco;
    }

    /**
     * getting the Paper
     * @return boolean
     */
    public boolean hasPaper() {
        return hasPaper;
    }

    /**
     * Method that takes MATCHES from the table or says that there are none
     * @return MATCHES
     */
    public synchronized Resource getMatches() {
        if (hasMatches()) {
            hasMatches = false;
            System.out.println("We took matches");
            return Resource.MATCHES;
        } else {
            System.out.println("No matches");
            return null;
        }
    }

    /**
     * Мethod that takes TOBACCO from the table or says that there are none
     * @return TOBACCO
     */
    public synchronized Resource getTobacco() {
        if (hasTobacco()) {
            hasTobacco = false;
            System.out.println("Took tobacco");
            return Resource.TOBACCO;
        } else {
            System.out.println("No tobacco");
            return null;
        }
    }
    /**
     * Method that takes PAPER from the table or says that there are none
     * @return PAPER
     */
    public synchronized Resource getPaper() {
        if (hasPaper()) {
            hasPaper = false;
            System.out.println("They took the paper");
            return Resource.PAPER;
        } else {
            System.out.println("No paper");
            return null;
        }
    }
    /**
     * Method adds resources to the table
     * @param resources
     */
    public void addResources(ArrayList<Resource> resources) {
        for (Resource resource : resources) {
            switch (resource) {
                case MATCHES:
                    if (!hasMatches) {
                        System.out.println("Matches are placed on the table");
                        hasMatches = true;
                    }
                    break;
                case TOBACCO:
                    if (!hasTobacco) {
                        System.out.println("Tobacco was placed on the table");
                        hasTobacco = true;
                    }
                    break;
                case PAPER:
                    if (!hasPaper) {
                        System.out.println("Paper placed on the table");
                        hasPaper = true;
                    }
                    break;
            }
        }
    }

}
