package Program3;

/** Resources that smokers may have */
public enum Resource {
    MATCHES, TOBACCO, PAPER
}
