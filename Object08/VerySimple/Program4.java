package VerySimple;

public class Program4 {
    public static void main(String[] args) {
        int[][] matrix = {
            {8, 8, 8, 8},
            {1, 1, 1, 1},
            {2, 2, 2, 2},
            {3, 3, 3, 3}};
        int m = matrix.length;
        //System.out.println(c.length);
        for (int i = 0; i < m; i++) {
            for (int j = i + 1; j < m; j++){
                int temp = matrix[i][j];
                matrix[i][j] = matrix[j][i];
                matrix[j][i] = temp;
            }
        }
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < m; j++){
                System.out.print(matrix[i][j]);
            }
            System.out.println("");
        }
    }
}