package Program2;

import java.math.BigInteger;

public class FactorialRec {
    public static BigInteger Rec(int n) {
        BigInteger result = BigInteger.ONE;
        
        if(n == 1){
            return BigInteger.valueOf(1);
        } else {
            result = BigInteger.valueOf(n).multiply(Rec(n - 1));
            return result;
        }
    }
}
