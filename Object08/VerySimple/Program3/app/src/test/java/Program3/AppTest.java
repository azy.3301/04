package Program3;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.math.BigInteger;

class AppTest {
    @Test void sum() {

        int num = Sum.sum(100);
        int dispersionTnum = 5050;

        assertEquals(dispersionTnum, num);
    }
}
