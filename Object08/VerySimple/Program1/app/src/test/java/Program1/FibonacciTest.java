package Program1;

import org.junit.jupiter.api.Test;
 
import static org.junit.jupiter.api.Assertions.*;

public class FibonacciTest {
    @Test void fibonacci() {
        int num = 20;
		int[] fibo = Fibonacci.fibonacci(num);
        int number = fibo[19];
        int dispersionNum = 4181;

        assertEquals(number, dispersionNum);
    }

    @Test void getFibo() {
        int num = 43;
		int[] fiboRec = FibonacciRec.getFibo(num);
        int number = fiboRec[42];
        int dispersionNum = 267914296;

        assertEquals(number, dispersionNum);
    }
}
