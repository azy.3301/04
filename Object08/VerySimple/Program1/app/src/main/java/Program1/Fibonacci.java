package Program1;

public class Fibonacci {
    public static int[] fibonacci(int num){
        int n0 = 1, n1 = 1, n2;
        int[] fibo = new int[num];
        fibo[0] = 0; fibo[1] = n0; fibo[2] = n1;
		for(int i = 3; i < num; i++){
			n2 = n0 + n1;
            fibo[i] = n2;
			n0 = n1;
			n1 = n2;
		}
        return fibo;
    }
}
