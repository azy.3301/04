package Program1;

public class FibonacciRec {
    public static int fibo(int n, int[] arr){
        if (arr[n - 1] != 0){
            return arr[n - 1];
        }
        if (n <= 2){
            arr[n - 1] = n - 1;
            return arr[n - 1];
        }
        int result = fibo(n - 1, arr) + fibo(n - 2, arr);
        arr[n - 1] = result;
        return result;
       }

    public static  int[] getFibo(int n){
        if (n == 0){
          return null;
        }
        int[] arrNew = new int[n];
        fibo(n, arrNew);
        return arrNew;
    }
}
