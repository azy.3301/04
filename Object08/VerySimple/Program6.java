package VerySimple;

public class Program6 {
    public static void main(String[] args) {
        int[][] matrix = {
            {8, 8, 8, 8},
            {1, 1, 1, 1},
            {2, 2, 2, 2},
            {3, 3, 3, 3}};
        int m = matrix.length;
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < i; j++){
                System.out.print(matrix[i][j]);
            }
            System.out.println("");
        }
    }
}
