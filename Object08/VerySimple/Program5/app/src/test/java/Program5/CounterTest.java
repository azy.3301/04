package Program5;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;


class AppTest {
    @Test void counter() {

        char a = 'l';
        String str = "Hello_world";

        int sum = Counter.counter(str, a);
        int correctSum = 3;

        assertEquals(correctSum, sum);
    }
}
