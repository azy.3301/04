package Program5;

public class Counter {
    public static int counter(String str, char a){
        int i = 0;
        int sum = 0;
        while (i < str.length() - 1){
            i ++;
            char c = str.charAt(i);
            if (c == a) {
                sum ++;
            }
        }
        return sum;
    }
}
