package Simple.Program4;


public class Process {
    public static void process(double limit, double coefficient, long maxSimulationTime,long startTime){
        try {
            Thread.sleep((int) (Math.random() * 1000));
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        double genNum = (double) (Math.random() * 2 * limit * coefficient - limit * coefficient);
        long SimulationTime = System.currentTimeMillis() - startTime;

        if ((maxSimulationTime >= SimulationTime) && (limit >= genNum)){
            process(limit, coefficient, maxSimulationTime, startTime);
        } else {
            System.out.println(SimulationTime + " : " + genNum);
        }
    }
}
