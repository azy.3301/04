package Simple.Program4;

import java.util.Scanner;

public class Main {
    static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        System.out.print("Введите порог: ");
        double limit = scanner.nextDouble();
        System.out.print("Введите коэфиценнт: ");
        double coefficient = scanner.nextDouble();
        System.out.print("Введите максимальное время модеирования: ");
        long maxSimulationTime = scanner.nextLong();

        long startTime = System.currentTimeMillis();

        Process.process(limit, coefficient, maxSimulationTime, startTime);

    }

}
