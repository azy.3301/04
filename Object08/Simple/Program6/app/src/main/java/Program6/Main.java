package Program6;

import java.util.Scanner;

public class Main {
    static Scanner scanner = new Scanner(System.in);
    public static void main(String[] args) {
        System.out.print("Введите количество матриц: ");
        int number = scanner.nextInt();
        System.out.print("Введите размер матриц: ");
        int sizeMatrix = scanner.nextInt();
        System.out.println(number + " " + sizeMatrix);

        // Генервция матриц
        MatrixGeneration[] matrix = new MatrixGeneration[number];

        for (int i = 0; i < number; i++) {
            matrix[i] = new MatrixGeneration(sizeMatrix);
        }
        // Записываем нначальное время
        long time = System.currentTimeMillis();

        // Перемножения всех матриц
        MatrixMultiplication Multiplication = new MatrixMultiplication();
        int[][] matrixResult = matrix[0].getMatrix();

        for (int i = 1; i < number; i++){
            matrixResult = Multiplication.matrixMultiplication(matrixResult, matrix[i].getMatrix());
        }

        // Выводим время затрачиное на перемножение матриц.
        System.out.println((System.currentTimeMillis() - time) + " Milliseconds");

        // Вывод матрицы
        for (int i = 0; i < sizeMatrix; i++) {
            for (int j = 0; j < sizeMatrix; j++) {
                System.out.print(" " + matrixResult[i][j] + " ");
            }
            System.out.println();
        }
        System.out.println();
    }

}
