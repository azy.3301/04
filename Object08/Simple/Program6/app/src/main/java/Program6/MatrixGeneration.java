package Program6;

public class MatrixGeneration {
    private int[][] matrix;

    public MatrixGeneration(int sizeMatrix) {
        int[][] mat = new int[sizeMatrix][sizeMatrix];
        for (int i = 0; i < sizeMatrix; i++){
            for (int j = 0; j < sizeMatrix; j++){
                mat[i][j] = (int) (Math.random() * 20);;
            }
        }
        this.matrix = mat;
    }

    public int[][] getMatrix(){
        return matrix;
    }


}
