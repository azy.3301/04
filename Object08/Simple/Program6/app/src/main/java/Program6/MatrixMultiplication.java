package Program6;

public class MatrixMultiplication {

    public int[][] matrixMultiplication(int[][] matrix1, int[][] matrix2) {
        int[][] matrixresultNew = new int[matrix1.length][matrix2[0].length];

        for (int i = 0; i < matrix1.length; i++) {
            for (int j = 0; j < matrix2[i].length; j++) {
                int sum = 0;
                for (int k = 0; k < matrix2[j].length; k++) {
                    sum += (matrix1[i][k] * matrix2[k][j]);
                }
                matrixresultNew[i][j] = sum;
            }
        }
        return matrixresultNew;
    }

}
