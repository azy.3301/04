package Menu;

import java.util.Scanner;

import Menu.Action.*;

public class Menu {

    static Scanner scanner = new Scanner(System.in);
    public static void main(String[] args) {
        Node node0 = new Node(2, new Action("\n Поиск произведений. Версия 1.1. \n", MessageSet.INFORMATIONT_DESCRIPTION, 2));

        Node node1 = new Node(2, new Action("\n Осип Эмильевич Мандельштам - русский поэт, прозаик и переводчик, эссеист, критик, литературовед. Один из крупнейших русских поэтов XX века \n", MessageSet.MANDELSTAM_DESCRIPTION, 2));

        Node node2 = new Node(2, new Action("\n Иосиф Александрович Бродский - русский и американский поэт, эссеист, драматург и переводчик, педагог. Лауреат Нобелевской премии по литературе 1987 года, поэт-лауреат США в 1991—1992 годах. Стихи писал преимущественно на русском языке, эссеистику — на английском. Почётный гражданин Санкт-Петербурга (1995). \n", MessageSet.BRODSKY_DESCRIPTION, 2));
        
        Node node11 = new Node(0, new Action("\n Там — я любить не мог, \n Здесь — я любить боюсь.. \n", MessageSet.TEXT_DESCRIPTION, 0));

        Node node12 = new Node(0, new Action("\n Паденье — неизменный спутник страха,\n И самый страх есть чувство пустоты. \n", MessageSet.TEXT_DESCRIPTION, 0));

        Node node21 = new Node(0, new Action("\n Скука – наиболее распространенная черта существования \n", MessageSet.TEXT_DESCRIPTION, 0));
        
        Node node22 = new Node(0, new Action("\n Смерть - это то, что бывает с другими \n", MessageSet.TEXT_DESCRIPTION, 0));
        node0.children[0] = node1;
        node0.children[1] = node2;
        node1.setParent(node0);
        node2.setParent(node0);
        node1.children[0] = node12;
        node1.children[1] = node11;
        node2.children[0] = node22;
        node2.children[1] = node21;
        node12.setParent(node1);
        node11.setParent(node1);
        node21.setParent(node2);
        node22.setParent(node2);
        Node node = node0;
        int userInput = 0;

        while(true){
            System.out.println(node.getItem());
            for (int i = 0; i < node.getChildren().length; i++) {
                System.out.println(MessageSet.PRESS + (i + 2) + MessageSet.THEN + node.getChildren()[i].getAction().getDescription());
            }
            userInput = scanner.nextInt();
            System.out.println();
            if (userInput == 0){
                if (node.hasParent()){
                    break;
                }
                else{
                    node = node.getParent();
                }
            }
            if (userInput > 0 && userInput <= node.sizeAction()){
                node.doAction();
            }
            if (userInput > node.sizeAction() && userInput <= node.sizeChildren()){
                node = node.goToChild(userInput - node.sizeAction());
            }
        }
    }
}
