package Menu;


public class MessageSet {
    public static final String MOVE_UP = "Нажмите 0, чтобы поднятся выше";
    public static final String PRESS = "Нажмите ";
    public static final String THEN = ", чтобы ";

    // Action0
    public static final String INFORMATIONT_DESCRIPTION = "вывести описание";

    // Action1
    public static final String MANDELSTAM_DESCRIPTION = "посмотреть Мандельштама";

    // Action2
    public static final String BRODSKY_DESCRIPTION= "посмотреть Бродского";

    // Action 11/12/21/22
    public static final String TEXT_DESCRIPTION= "посмотреть цытату";
}
