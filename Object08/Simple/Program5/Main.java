package Simple.Program5;

import java.util.Scanner;

public class Main {
    static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        System.out.print("Введите количество повторений: ");
        int repetitions = scanner.nextInt();
        System.out.print("Введите размер области: ");
        int areaSize = scanner.nextInt();
        System.out.print("Введите сдвиг: ");
        int shift = scanner.nextInt();

        int intersect = 0;

        for (int i = 0; i < repetitions; i++){
            intersect += Process.process(repetitions, areaSize, shift);
          }
          System.out.println("Пересекаются: " + intersect);

    }

}
