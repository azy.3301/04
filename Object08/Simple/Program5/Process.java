package Simple.Program5;


public class Process {
    public static int process(int repetitions, int areaSize, int shift){
        int intersect = 0;

        int segmentStart1 = (int) (Math.random()* areaSize + shift);
        int endSegment1 = (int) (Math.random()* (areaSize - segmentStart1 + shift) + segmentStart1);

        int segmentStart2 = (int) (Math.random()* areaSize + shift);
        int endSegment2 = (int) (Math.random()* (areaSize - segmentStart2 + shift) + segmentStart2);

        if ((endSegment2 - segmentStart1 >= 0) || (endSegment1 - segmentStart2 <= 0)){
            intersect++;
        }

        return intersect;
    }
}
