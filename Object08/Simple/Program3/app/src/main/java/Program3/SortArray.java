package Program3;

public class SortArray {

    public void sort(double[] arr){
        boolean swapped;
        for(int i = arr.length-1 ; i > 0 ; i--){
            swapped = false;
            for(int j = 0 ; j < i ; j++){
                if( arr[j] > arr[j+1] ){
                    double tmp = arr[j];
                    arr[j] = arr[j+1];
                    arr[j+1] = tmp;
                    swapped = true;
                }
            }
            if (swapped == false)
                break;
        }
    }

    public double[] createSortedArray(double[] arr){
        double[] newArray = new double[arr.length];
        for(int q = arr.length - 1; q > -1; q--){
            newArray[q] = arr[q];
        }
        sort(newArray);
        return newArray;
    }
}
