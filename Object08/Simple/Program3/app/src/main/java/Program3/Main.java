package Program3;

import java.util.Arrays;

class Main{
    public static void main(String[] args) {
        double[] array = {12, 32, 3, 1, 3, 90, 8, 9, 91, 43, 8};
        double[] array2 = {0, 32, 3, 1, 3, 90, 8, 9, 91, 43, 8, 99};


        SortArray sortArray = new SortArray();
        sortArray.sort(array);
        System.out.println(Arrays.toString(array));
        System.out.println("-----------");
        System.out.println(Arrays.toString(sortArray.createSortedArray(array2)));
    }
}