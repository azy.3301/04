package Program1;

import java.util.Scanner;

//Создать программу, позволяющую вводить с клавиатуры число, сохранять его в целочисленной переменной и затем выводить переменную на консоль. Надо учесть, что пользователь может ввести любую строку. Если среди значащих символов есть только цифры, + или -, то строка должна быть преобразована в число.

public class Main {

    private static Scanner scanner;

    public static void main(String[] args) {
        scanner = new Scanner(System.in);
        System.out.print("Введите число: ");
        String str = scanner.nextLine();
        int num = 0;

        if (CheckString.isNumber(str)) {
            num = Integer.parseInt (str);
            System.out.println("Число: " + num);
        } else {
            System.out.println(str + " не число");
        }
    }
}
