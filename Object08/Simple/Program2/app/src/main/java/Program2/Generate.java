package Program2;

public class Generate {
    private int[] genNumbers;
    private long genTime;
    private long limitTime = 5000;

    public int[] setGenNumbers(){
        return genNumbers;
    }

    public long setGenTime(){
        return genTime;
    }

    public int[] generateArray(int length){
        int[] arr = new int[length];
        for (int i = 0; i < length - 1; i++){
            arr[i] = ((int) (Math.random() * 20));
        }
        return arr;
    }

    public boolean gen(int num){
        long StartTime = System.currentTimeMillis();
        generateArray(num);
        this.genTime = System.currentTimeMillis() - StartTime;
        if (limitTime > genTime){
            System.out.println(genTime);
            return false;
        } else {
            System.out.println("превышено допустимое время работы программы, введите количество поменьше:");
            return true;
        }
    }
}