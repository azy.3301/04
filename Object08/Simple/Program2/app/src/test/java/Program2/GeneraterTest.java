package Program2;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;


class GenerateTest {
    Generate generat = new Generate();

    @Test void generateArray() {

        int[] genArray = generat.generateArray(888);
        int lengArray = genArray.length;
        int correctLengArray = 888;

        assertEquals(correctLengArray, lengArray);
    }

    @Test void genFalse() {

        boolean x = generat.gen(2);

        assertEquals(false, x);
    }

    @Test void genTrue() {

        boolean x = generat.gen(99999999);

        assertEquals(true, x);
    }
}
