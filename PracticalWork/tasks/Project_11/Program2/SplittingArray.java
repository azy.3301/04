package Project_11.Program2;

import java.util.Arrays;

public class SplittingArray {
    public static void main(String args[]) {
        int sizeArray = 8;
        int [] array = new int[sizeArray];
        System.out.println("Enter elements of the array");
        for(int i = 0; i < sizeArray; i++) {
            array[i] = ((int) (Math.random() * 100));
        }
        System.out.println(Arrays.toString(array));
        int k = 5;
        int l = array.length / k;
        int start = 0; int end = l;
        int[][] newArray = new int[k][];
        for (int i = 0; i < k - 1; i++) {
            newArray[i] = CopyOfRangeCustom.copyOfRangeCustom(array, start, end);
            start += l;
            end += l;
        }
        newArray[k - 1] = CopyOfRangeCustom.copyOfRangeCustom(array, start, array.length);

        for (int i = 0; i < newArray.length; i++){
            System.out.println(Arrays.toString(newArray[i]));
        }
    }
}