package Project_2;

//сдвинуть элементы на заданное количество позиций без переноса (пустые места заполнять нулями)
//import java.util.Arrays;

public class Program2 {

    public static void main(String[] args) {
        int [] array = new int[] {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
        int n = 3;
        int [] arraySort = new int[array.length];
        for (int i = 0 ; i <= n - 1; i++) {
            arraySort[i] = 0;
        }
        int x = 0;
        for (int i = n; i < array.length; i++) {
            arraySort[i] = array[x];
            x++;
        }
        //System.out.println(Arrays.toString(array));
        for (int i = 0; i < array.length; i++) {
            System.out.print(arraySort[i] +  " ");
        }
    }
}
