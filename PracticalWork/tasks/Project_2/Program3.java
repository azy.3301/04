package Project_2;

//поменять местами элементы в каждой паре
//import java.util.Arrays;

public class Program3 {
    public static void main(String[] args) {
        int [] array = new int[] {1, 2, 3, 4, 5, 6, 7};
        if (array.length % 2 == 0){
            for (int i = 0; i < array.length; i++) {
                int temp = array[i];
                array[i] = array[i + 1];
                array[i + 1] = temp;
                i++;
            }
        } else {
            for (int i = 0; i < array.length - 1; i++) {
                int temp = array[i];
                array[i] = array[i + 1];
                array[i + 1] = temp;
                i++;
            }
        }
        //System.out.println(Arrays.toString(array));
        for (int i = 0; i < array.length; i++) {
            System.out.print(array[i] +  " ");
        }
    }
}
