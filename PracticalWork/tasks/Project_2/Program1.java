package Project_2;

//поменять знак у всех элементов, которые больше 10

//import java.util.Arrays;

public class Program1 {
    public static void main(String[] args) {
        int [] array = new int[] {1, 11, 3, 4, 5, 6};
        for (int i = 0; i < array.length; i++) {
            if (array[i] > 10) {
                array[i] = array[i] * -1;
            }
        }
        //System.out.println(Arrays.toString(array));
        for (int i = 0; i < array.length; i++) {
            System.out.print(array[i] +  " ");
        }
    }
}