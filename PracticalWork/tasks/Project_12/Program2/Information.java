package Project_12.Program2;

public class Information {
    public String fullName;
    public int result;

    public Information(String name, int res) {
        this.fullName = name;
        this.result = res;
    }

    public void showInformation(){
        System.out.println(fullName + " Result " + result + "sec");
    }
}


