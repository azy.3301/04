package Project_12.Program2;

public class Info {
    private String name;
    private String surname;
    private String otchestvo;
    private String fio;
    public int result;

    public Info(String name, String surname, String otchestvo){
        this.name = name;
        this.surname = surname;
        this.otchestvo = otchestvo;
        this.result = (int) (Math.random() * 100);
    }

    public void showInfo(){
        this.fio = this.surname + " " + this.name + " " + this.otchestvo;
        System.out.println("Студент " + this.fio + " пробежал дистанцию за " + this.result + "сек.");
    }
}
