package Project_12.Program4;

import java.io.IOException;

public class Streams {
    public static void main(String[] args) throws IOException {
        long startTime = System.currentTimeMillis();

        System.out.println("Main Started");

        StartThread[] arrThreads = new StartThread[10];

        for(int i=1; i < arrThreads.length; i++){
            arrThreads[i] = new StartThread("newThread" + i);
        }

        for(int i=1; i < arrThreads.length; i++){
            arrThreads[i].start();
        }

        try {
            for(int i = 1; i < arrThreads.length; i++){
                arrThreads[i].join();
            }

            System.out.println("Main Stop");

            System.out.println((double) (System.currentTimeMillis() - startTime) / 1000);
        } catch (InterruptedException e) {
            System.out.println("error");
        }
    }
}
