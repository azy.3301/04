package Project_12.Program4;

class StartThread extends Thread {

    StartThread(){
    }

    StartThread(String name){
        super(name);
    }

    public void run(){

        System.out.printf(Thread.currentThread().getName() + " started... \n");

        try{

            Thread.sleep(5000 + (int) (Math.random() * 5001));
        }
        catch(InterruptedException e){
            System.out.println("Interrupted");
        }
        System.out.printf("%s fiished... \n", Thread.currentThread().getName());
    }
}