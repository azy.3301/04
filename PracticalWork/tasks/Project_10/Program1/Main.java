//Описать классы воина, мага и птицы. Все должны уметь перемещаться, у всех есть здоровье, маги и птицы должны уметь летать, причем это поведение должно быть одинаковым для них. В основной программе создать массив объектов этих классов, летающим дать возможность полетать.

package Project_10.Program1;

public class Main {
    public static void main(String[] arge){
        Heroes[] herpes = new Heroes[3];

        // herpes[0] = new Magician();
        herpes[1] = new Bird();
        herpes[2] = new Warrior();

        int r = ((AbilityToFly) herpes[1]).Fly(2);
        System.out.println(r);
    }
}
