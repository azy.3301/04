package Project_6;

//создать строку случайной длины из символов "b" и вывести ее.

public class Program2 {

    public static void main(String[] args) throws Exception {
        String strB = "b".repeat((int) (Math.random() * 100));  // link javadoc repeat - https://docs.oracle.com/en/java/javase/11/docs/api/java.base/java/lang/String.html#repeat(int) || link javadoc random - https://docs.oracle.com/javase/8/docs/api/java/lang/Math.html#random--

        System.out.println(strB);
    }
}
