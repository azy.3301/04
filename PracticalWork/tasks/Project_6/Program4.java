package Project_6;

//дана строка из букв и пробелов, найти количество слов в ней.

public class Program4 {

    public static void main(String[] args) throws Exception {

        //string generator

        String alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"; //0123456789
        int lineLength = 100;
        String randomStr = "";

        for(int i = 0; i < lineLength; i++){
            int n = (int) (Math.random() * 2);
            if (n == 1){
                randomStr += alphabet.charAt((int) (Math.random() * alphabet.length()));
            } else {
                randomStr += " ";
            }

        }
        System.out.println(randomStr);

        //Word counter

        //String n = " dd sd sd      sd   s";
        int counter = 0;
        for(int i = 0; i < randomStr.length() - 1; i++){
            if ((randomStr.charAt(i) == ' ') && (i < randomStr.length() - 1)) {
                counter ++;
                i ++;
                while ((randomStr.charAt(i) == ' ') && (i < randomStr.length() - 1)){
                    i ++;
                }
            }
        }
        System.out.println(counter);
    }
}
