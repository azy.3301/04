package Project_6;

//создать массив строк, каждая строка состоит из 10 символов "a". вывести строки на экран.

import java.util.Arrays; // linc javadoc class Arrays - https://docs.oracle.com/en/java/javase/11/docs/api/java.base/java/util/Arrays.html

public class Program1{
    public static void main(String[] args) throws Exception {
        String[] arrStr = new String[10];
        for (int i = 0; i < arrStr.length; i++){
            arrStr[i] = "a".repeat(10);    // link javadoc repeat - https://docs.oracle.com/en/java/javase/11/docs/api/java.base/java/lang/String.html#repeat(int)
        }
    System.out.print(Arrays.toString(arrStr));
    }
}