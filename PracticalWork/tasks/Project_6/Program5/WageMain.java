package Project_6.Program5;

//описать класс, у которого есть одно целочисленное скрытое поле (белая зарплата, например) и еще одно целочисленное поле (черная премия). Метод получения зарплаты показывает белую, просматривать черную премию нельзя, но ее можно устанавливать. Метод преобразования объекта в строку должен выводить полную зарплату.

public class WageMain {
    public static void main(String[] args) throws Exception {

        Wage wage = new Wage();
        System.out.println(wage.sum());

    }
}
