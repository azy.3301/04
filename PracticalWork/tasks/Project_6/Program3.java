package Project_6;

//создать строку из 100 случайных символов английского алфавита.

public class Program3 {
    public static void main (String[] args) throws Exception {
        String alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"; //0123456789
        int lineLength = 100;
        String randomStr = "";
        for(int i = 0; i < lineLength; i++){
            randomStr += alphabet.charAt((int) (Math.random() * alphabet.length()));
        }

        System.out.println(randomStr);
    }

}
