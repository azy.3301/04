package Project_3;

//массив вещественных чисел заполнен по формуле a[i] = i * i / k, k - заданное число. С какого индекса значение элемента начинает превышать значение индекса?
//import java.util.Arrays;

public class Program5 {
    public static void main(String[] args){
        long[] massiv = new long[9999];
        int k = 9;
        int index = 0;
        for (int i = 1; massiv[i - 1] < k; i++){
            massiv[i] = i * i / k;
            index = i;
        }
        System.out.print(index);
    }
}
