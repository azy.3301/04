package Project_3;

// найти среднее арифметическое элементов массива

// import java.util.Arrays;

public class Program1 {
    public static void main(String[] args) {
        double [] array = new double [] {0, 1, 0, 0, 0, 1};
        double sum = 0;
        for (int i = 0; i < array.length; i++) {
            sum += array[i];
        }
        //System.out.println(Arrays.toString(array));
        System.out.format("%.2f", sum / array.length);

    }
}