package Project_3;

//найти дисперсию положительных элементов массива
//import java.util.Arrays;

public class Program2 {

    public static void main(String[] args) {
        int [] array = new int[] {1, 11, 3, 4, 5};
        double sum = 0;
        double average = 0;

        for (int i = 0; i < array.length; i++) {
            sum += array[i];
        }
        average = sum / array.length;
        double des = 0;
        for (int i = 0; i < array.length; i++) {
            des += (array[i] - average) * (array[i] - average);
        }
        des /= array.length;
        System.out.format("%.2f", des);

    }
}
