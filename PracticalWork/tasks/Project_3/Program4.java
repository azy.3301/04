package Project_3;

//какое максимальное количество подряд идущих элементов находится в массиве? 0, 1, 3, 6, 7, 8 - в этом массиве подряд идут 0, 1 и 6, 7, 8. Максимальное количество - 3.
//import java.util.Arrays;

public class Program4 {

    public static void main(String[] args) {
        int [] array = new int[] {1, 2, 3, 4, 2, 3, 0, 5, 6, 7, 8, 4, 5, 6, 7, 8};
        int num = 0;
        int max = 1;
        int totalMax = 0;
        int numNext = array[1];
        for (int i = 0; i < array.length; i++) {
            num = array[i] + 1;
            if (num == numNext){
                max += 1;
            } else if (totalMax < max) {
                totalMax = max;
                max = 1;
            } else {
                max = 1;
            }
            if (i > array.length - 3){
                break;
            } else {
                numNext = array[i + 2];
            }
        }
        if (totalMax < max){
            totalMax = max;
        }
        System.out.println(totalMax);
    }
}
