package Project_5.Program3;

//описать свой класс для целых чисел, который будет хранить числа и позволять их складывать с помощью метода экземпляра. в программе создать два экземпляра и вывести результат сложения.

class Program3{
  public int num;

  Program3(int num){
    this.num = num;
  }
  public int Sum(Program3 num){
    return this.num + num.num;
  }
}
