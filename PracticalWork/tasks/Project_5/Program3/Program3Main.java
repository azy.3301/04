package Project_5.Program3;

//описать свой класс для целых чисел, который будет хранить числа и позволять их складывать с помощью метода экземпляра. в программе создать два экземпляра и вывести результат сложения.

class Program3Main{
  public static void main(String[] args) {
    Program3 num1 = new Program3(5);
    Program3 num2 = new Program3(4);
    System.out.println(num1.Sum(num2));
  }
}
