package Project_5.Program1;

//есть двумерный массив целых чисел, длина которого случайное число и длина вложенного массива также случайное число. посчитать дисперсию длин вложенных массивов.

//import java.util.Arrays;

public class Program1 {

    public static void main(String[] args) {
        int[][] a = new int[(int)(Math.random()*10)+1][];
        for(int i = 0; i < a.length; ++i){
            a[i] = new int[(int)(Math.random()*10)+1];
        }

        double mean = 0;
        for(int i = 0; i < a.length; ++i){
            mean += a[i].length;
        }

        mean /= a.length;

        double des = 0;
        for(int i = 0; i < a.length; ++i){
            des += (a[i].length - mean)*(a[i].length - mean);
        }
        des /= (a.length - 1);
        System.out.format("%.2f", des);
    }
}
