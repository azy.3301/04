package Project_5.Program2;

//описать класс, у которого есть одно целочисленное поле, которое заполняется случайным числом при создании экземпляра. создать массив из 10 экземпляров этого класса и вывести элементы, у которых значение этого поля больше 100.

class Program2Main{
  public static void main(String[] args) {
    int len = 10;
    Program2[] arr = new Program2[len];
    for (int n = 0; n < len; n++) {
      arr[n] = new Program2();
    }
    for (int i = 0;i < len; i++) {
      if (arr[i].FieldInt > 100) {
        System.out.println(arr[i]);
      }
    }
  }
}
