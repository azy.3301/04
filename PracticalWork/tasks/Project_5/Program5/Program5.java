package Project_5.Program5;

//описать класс, который содержит два целочисленных поля, они должны заполняться случайными числами при создании экземпляра. создать массив из 10 экземпляров этого класса, вывести количество элементов, у которых разница значений полей не превышает 5.

class Program5{
  public int[] arr;
  Program5(int n){
    arr = new int[n];
    for (int i = 0;i < n; i++) {
      this.arr[i] = (int) (Math.random() * 10);
    }
  }
  public double Aver(){
    double k = 0;
    for (int i = 0;i < this.arr.length; i++) {
      k = k + (double) this.arr[i];
    }
    return k / this.arr.length;
  }
}
