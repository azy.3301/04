package Project_4;

//сколько элементов больше минимального на 5?

//import java.util.Arrays;

public class Program4 {

    public static void main(String[] args) {
        int [] array = new int[] {1, 11, 3, 4, 5};
        int min = array[0];

        for (int i = 1; i < array.length; i++) {
            if (min > array[i]) {
                min = array[i];
            }
        }
        int num = 0;

        for (int i = 0; i < array.length; i++) {
            if (min >= array[i] - 5) {
                num += 1;
            }
        }
        System.out.print(num - 1);
    }

}
