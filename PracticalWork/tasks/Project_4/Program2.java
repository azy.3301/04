package Project_4;

import java.util.Arrays;

import Project_4.Program1.Program1;

public class Program2 {

    public static void main(String[] args) {
        Program1[] array =  new Program1[5];

        for (int i = 0; i < array.length; i++) {
            array[i] = new Program1();
        }

        array[array.length - 1].second = 23;
        array[array.length - 1].first = 32;

        System.out.println(Arrays.toString(array));
    }
}
