package Project_4;

//создать массив из 10 целых случайных чисел

import java.util.Arrays;

public class Program5 {
    public static void main(String[] args){
        int [] array = new int[10];
        for (int i = 0; i < array.length; i++) {
            array[i] = (int) (Math.random() * 100);
        }

        System.out.println(Arrays.toString(array));
    }
}
