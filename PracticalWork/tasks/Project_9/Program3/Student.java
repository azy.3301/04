package Project_9.Program3;

public class Student {

  private String name;
  public String group;
  public int count;

  public Student(String name, String group) {
    this.name = name;
    this.group = group;
    count++;
  }
  public int getCount() {
    return count;
  }
  public String getName() {
    return name;
  }
}
