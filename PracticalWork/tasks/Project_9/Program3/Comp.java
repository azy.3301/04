package Project_9.Program3;

import java.util.Comparator;

public class Comp implements Comparator<Student> {
    public int compare(Student st1, Student st2) {
        if (st1.getName().length() - st2.getName().length() > 0)
            return 1;
        else if (st1.getName().length() - st2.getName().length() == 0)
            return 0;
        else
            return -1;
    }
}
