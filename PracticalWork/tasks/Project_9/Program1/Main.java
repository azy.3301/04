package Project_9.Program1;

//В задаче производителей и потребителей сделать возможность пользователю вводить строку, которую использует производитель. Если место для записи свободно, но пользователь еще не ввел значение, то генерировать случайную строку любым способом. При вводе пустой строки останавливать выполнение программы.


import java.util.Scanner;

public class Main{
    private static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) throws Exception {
        String alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
        String randomStr = "";
        for(int i = 0; i < 10; i++){
            randomStr += alphabet.charAt((int) (Math.random() * alphabet.length()));
        }

        Manufacturer manufacturer = new Manufacturer(randomStr);

        for (; ;) {
            String str = scanner.nextLine();
            if (str == ""){
                break;
            } else {
                manufacturer.ReadStr(str);
            }
            System.out.println("0");
        }
        String manufacturerStr = manufacturer.getStr();
        System.out.println(manufacturerStr);
    }
}