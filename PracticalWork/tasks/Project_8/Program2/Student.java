package Project_8.Program2;

public class Student {

    private String name;
    private int group;
    private int number;

    public Student(String name, int group, int number) {
        this.name = name;
        this.group = group;
        this.number = number;
    }

    public Student() {
    }

    public void sayMeow() {
        System.out.println("Ура!!!");
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getGroup() {
        return group;
    }

    public void setGroup(int group) {
        this.group = group;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }
 }