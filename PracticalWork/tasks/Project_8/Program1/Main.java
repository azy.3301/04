package Project_8.Program1;

public class Main {
    public static void main(String[] args) {

        // Student Alex = new Student("Alex", 5, 0);
        // Student Vika = new Student("Vika", 4, 4);
        // Student Kat = new Student("Kat", 4, 4);
        // Student Lexa = new Student("Lexa", 4, 4);
        // Student Jon = new Student("Jon", 4, 4);

        Student[] ArrStudent =  new Student[5];
        int ArrStudentLength = ArrStudent.length;

        ArrStudent[0] = new Student("Jon", 4, ArrStudentLength);
        ArrStudent[1] = new Student("Oliver", 5, ArrStudentLength);
        ArrStudent[2] = new Student("Kat", 4, ArrStudentLength);
        ArrStudent[3] = new Student("Vika", 4, ArrStudentLength);
        ArrStudent[4] = new Student("Alex", 5, ArrStudentLength);

        for (int i = 0; i < ArrStudent.length; i++) {
            ArrStudent[i].sayMeow();
        }


        String OliverName = ArrStudent[1].getName();
        int OliverGroup = ArrStudent[1].getGroup();
        int OliverNumbe = ArrStudent[1].getNumber();

        System.out.println("Naim: " + OliverName);
        System.out.println("Group: " + OliverGroup);
        System.out.println("Number: " + OliverNumbe);
        ArrStudent[1].setGroup(4);
        OliverGroup = ArrStudent[1].getGroup();
        System.out.println("Group: " + OliverGroup);
    }
 }