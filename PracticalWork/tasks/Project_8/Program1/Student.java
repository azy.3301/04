package Project_8.Program1;

public class Student extends Human{

    private String name;
    public int group;
    public int number;

    public Student(String name, int group, int number) {
        this.name = name;
        this.group = group;
        this.number = number;
    }

    public Student() {
    }

    public String getName() {
        return name;
    }

    public void  setName() {
    }

    public int getGroup() {
        return group;
    }

    public void setGroup(int group) {
        this.group = group;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }
 }