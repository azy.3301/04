package Project_8.Program1;

public class Employee extends Human{
    private String name;
    private String position;
    public int number;

    public Employee(String name, String position, int number) {
        this.name = name;
        this.position = position;
        this.number = number;
    }

    public void Student() {
    }

    public String getName() {
        return name;
    }

    public void setName() {
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

}
