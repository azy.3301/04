package Program3;

public class Engineer extends Worker {

    public Engineer(String name, int salary) {
        super(name, salary, "Engineer");
    }

    @Override
    public String work(){
        return "Закручиваю гайки";
    }
}