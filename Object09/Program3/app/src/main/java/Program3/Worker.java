package Program3;

public abstract class Worker {
    private String name;
    private String position;
    private int salary;

    public Worker(String name, int salary, String position) {
        this.setName(name);
        this.setSalary(salary);
        this.setPosition(position);
    }

    public void setName(String name){
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setPosition(String position){
        this.position = position;
    }

    public String getPosition() {
        return position;
    }

    public void setSalary(int salary){
        this.salary = salary;
    }

    public int getSalary() {
        return salary;
    }

    public abstract String work();


    public String information() {
        return "Name: " + getName() + " " + "Position: " + getPosition() + " " + "Salary: " + getSalary();
    }

    public Object clone() throws CloneNotSupportedException{
        return super.clone();
    }

    public int hashCode() { 
        return (name.length() + position.length() + salary) / 2 * 10;
    }

    public String toString() {
        return "Name" + name;
    }

    public boolean equals(Object obj) {
        if (obj == this) return true;
        if (obj == null || obj.getClass() != this.getClass()) return false;
        Worker worker = (Worker) obj;
        return this.salary == worker.salary && this.position == worker.position && this.name == worker.name;
    }
}
