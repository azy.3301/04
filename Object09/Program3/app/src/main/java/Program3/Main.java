// Создать классы для описания работников: работник в общем, бухгалтер, главный бухгалтер, инженер, рабочий. Все имеют имя, должность и зарплату. Добавить характерные для каждого класса в отдельности методы. Переопределить все доступные методы класса Object адекватным образом. Создать массив, содержащий объекты всех классов описания работников, и вывести его на консоль.f

package Program3;


public class Main {
    
    public static void main(String[] args) {

        Worker[] worker = new Worker[4];

        worker[0] = new Accountant("Nikoli", 30000);
        worker[1] = new ChiefAccountant("Nika", 55000);
        worker[2] = new Employee("Dora", 77000);
        worker[3] = new Engineer("Nikita", 23000);

        for (int i = 0; i < worker.length; i++){
            System.out.println(worker[i] + "\n" + worker[i].information() + "\n" + worker[i].work() + "\n");
        }
    }
}
