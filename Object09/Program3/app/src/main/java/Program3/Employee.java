package Program3;

public class Employee extends Worker {

    public Employee(String name, int salary) {
        super(name, salary, "Employee");
    }

    @Override
    public String work(){
        return "Перекладываю коробки из точки А в точку Б";
    }
}