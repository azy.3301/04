package Program4;

public class Triangle extends Polygons {
    
    public Triangle(int sizeLine){
        super(sizeLine, 3);
        setPerimeter(sizeLine * 3);
    }
}
