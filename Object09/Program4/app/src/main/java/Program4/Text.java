package Program4;

public class Text implements Output{
    private String text;

    public Text(String text) {
        this.text = text;
    }

    public void getText(String text){
        this.text = text;
    }

    public String setText(){
        return text;
    }

    public String output(){
        return "Text = " + text;
    }
    
}
