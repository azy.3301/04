package Program4;

public class Polygons implements Output{
    
    private int perimeter;

    public Polygons(int sizeLine,int corners){
        this.perimeter = sizeLine * corners;
    }

    public void setPerimeter(int perimeter) {
        this.perimeter = perimeter;
    }
    public int getPerimeter() {
        return perimeter;
    }

    public String output(){
        return "Perimeter = " + perimeter;
    } 
}
