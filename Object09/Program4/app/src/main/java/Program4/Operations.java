package Program4;

public class Operations {

    public static int calculatingPerimeterOfAllPolygons(Output[] output){
        int sum = 0;
        for(int i = 0; i < output.length; i++){
            if (output[i] instanceof Polygons){
                sum += ((Polygons) output[i]).getPerimeter();
            }
        }
        return sum;
    }

    public static void outputArrauElemens(Output[] output){
        for (int i = 0; i < output.length; i++){
            System.out.println(output[i]);
        }
    }
}