// Создать класс для описания многоугольников в общем и треугольника в частности. Также создать класс для описания текста. Все классы должны иметь метод вывода на экран. Создать отдельный класс для операций над объектами. В классе должен быть метод вывода массива объектов на экран. Все объекты задавать в исходном коде (не меньше 3-х различных треугольников и двух текстов).

package Program4;

public class Main {
    public static void main(String[] args){
    
        Triangle triangle1 = new Triangle(3);
        Triangle triangle2 = new Triangle(4);
        Triangle triangle3 = new Triangle(4);
        Polygons polygons = new Polygons(4, 6);
        Text text1 = new Text("qweasd");
        Text text2 = new Text("qwdafsasdfeasd");
        Output[] ou = new Output[]{triangle1, triangle2, triangle3, polygons, text1, text2};
        
        Operations.outputArrauElemens(ou);
        System.out.println(Operations.calculatingPerimeterOfAllPolygons(ou));
    }
}