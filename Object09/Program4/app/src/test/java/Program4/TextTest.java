/*
 * This Java source file was generated by the Gradle 'init' task.
 */
package Program4;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

class TextTest {
    Text word = new Text("qwer");
    @Test void getText() {
        assertEquals(word.setText(), "qwer");
    }
}
