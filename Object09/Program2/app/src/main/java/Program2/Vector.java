package Program2;

public class Vector {
    private int a;
    private int b;

    public Vector(int a, int b) {
        this.a = a;
        this.b = b;
    }

    public int getA() {
        return this.a;
    }

    public int getB() {
        return this.b;
    }

    public static Vector sum(Vector v1, Vector v2) {
        return new Vector(v1.getA() + v2.getA(), v1.getB() + v2.getB());
    }

    public static Vector subtraction(Vector v1, Vector v2) {
        return new Vector((v1.getA() - v2.getA()), (v1.getB() - v2.getB()));
    }

    public static Vector multiplication(Vector v1, int scalar) {
        return new Vector((v1.getA() * scalar), (v1.getB() * scalar));
    }

    public static Vector division(Vector v1, int scalar) {
        return new Vector((v1.getA() / scalar), (v1.getB() / scalar));
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Vector vector = (Vector) o;
        return Double.compare(vector.a, a) == 0 && Double.compare(vector.b, b) == 0;
    }
}
