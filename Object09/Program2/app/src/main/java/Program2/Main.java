// Создать класс для описания точки (вектора) в двумерном пространстве. Реализовать сложение, вычитание векторов с помощью методов, умножение и деление вектора на скаляр также реализовать с помощью соответствующих методов. Реализовать набор примеров для демонстрации работы указанных методов.

package Program2;

import java.util.Arrays;

class Main{
    public static void main(String[] args){
        Vector vector1 = new Vector(2, 3);
        Vector vector2 = new Vector(1, 2);
        int scalar = 3;
        Vector[] newVector = new Vector[4];
        newVector[0] = Vector.sum(vector1, vector2);
        newVector[1] = Vector.subtraction(vector1, vector2);
        newVector[2] = Vector.multiplication(vector1, scalar);
        newVector[3] = Vector.division(vector1, scalar);

        for (int i = 0; i < newVector.length; i++){
            System.out.println(newVector[i].getA() + " " + newVector[i].getB());
        }
    }
}
