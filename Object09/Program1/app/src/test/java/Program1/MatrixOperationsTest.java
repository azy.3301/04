/*
 * This Java source file was generated by the Gradle 'init' task.
 */
package Program1;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

class MatrixOperationsTest {
    int[][] m1 = {{1, 2, 3}, {1, 2, 3}, {1, 2, 3}};
    int[][] m2 = {{1, 2, 3}, {1, 2, 3}, {1, 2, 3}};
    MatrixOperations matrixOperations = new MatrixOperations();

    @Test void matrixAddition() throws MatrixException, NullPointerException{
        Matrix mat1 = new Matrix(m1);
        Matrix mat2 = new Matrix(m2);
        int[][] newMatrix = matrixOperations.matrixAddition(mat1, mat2);
        int[][] matrix = {{2, 4, 6}, {2, 4, 6}, {2, 4, 6}};
        assertArrayEquals(matrix, newMatrix);
    }

    @Test void matrixSubtraction() throws MatrixException, NullPointerException{
        Matrix mat1 = new Matrix(m1);
        Matrix mat2 = new Matrix(m2);
        int[][] newMatrix = matrixOperations.matrixSubtraction(mat1, mat2);
        int[][] matrix = {{0, 0, 0}, {0, 0, 0}, {0, 0, 0}};
        assertArrayEquals(matrix, newMatrix);
    }

    @Test void matrixMultiplication() throws MatrixException, NullPointerException{
        Matrix mat1 = new Matrix(m1);
        Matrix mat2 = new Matrix(m2);
        int[][] newMatrix = matrixOperations.matrixMultiplication(mat1, mat2);
        int[][] matrix = {{6, 12, 18}, {6, 12, 18}, {6, 12, 18}};
        assertArrayEquals(matrix, newMatrix);
    }
}