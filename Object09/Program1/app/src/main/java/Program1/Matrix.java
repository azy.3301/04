package Program1;

public class Matrix {
    private int[][] matrix;

    public Matrix(int sizeMatrix){
        this.matrix = matrixGeneration(sizeMatrix);
    }

    public Matrix(int[][] matrix) throws MatrixException{
        int n = matrix.length;
        if (n != 0){
            int m = matrix[0].length;
            for (int i = 1; i < n; i++){
            if (matrix[i].length != m){
                throw new MatrixException("Subarrays are not equal");
            }  
        }
        this.matrix = matrix;
        } else {
            throw new MatrixException("Arrau is empty");
        }
    }

    public int[][] matrixGeneration(int sizeMatrix) {
        int[][] mat = new int[sizeMatrix][sizeMatrix];
        for (int i = 0; i < sizeMatrix; i++){
            for (int j = 0; j < sizeMatrix; j++){
                mat[i][j] = (int) (Math.random() * 20);;
            }
        }
        return mat;
    }

    public int[][] getMatrix(){
        return matrix;
    }


}
