package Program1;

import java.util.Scanner;

public class Main {
    static Scanner scanner = new Scanner(System.in);
    public static void main(String[] args) throws MatrixException, NullPointerException{
        System.out.print("Введите размер матриц: ");
        int sizeMatrix = scanner.nextInt();


        MatrixOperations matrixOperations = new MatrixOperations();

        // Генервция матриц
        Matrix matrix1 = new Matrix(sizeMatrix);
        Matrix matrix2 = new Matrix(sizeMatrix);
        // Сложение матриц
        int[][] matrixAdditionResult = matrixOperations.matrixAddition(matrix1, matrix2);

        // Вычитанние матриц
        int[][] matrixSubtractionResult = matrixOperations.matrixSubtraction(matrix1, matrix2);

        // Перемножения матриц
        int[][] matrixMultiplicationResult = matrixOperations.matrixMultiplication(matrix1, matrix2);

        // Вывод матрицы
        for (int i = 0; i < sizeMatrix; i++) {
            for (int j = 0; j < sizeMatrix; j++) {
                System.out.print(" " + matrixAdditionResult[i][j] + " ");
            }
            System.out.println();
        }
        System.out.println();

        // Вывод матрицы
        for (int i = 0; i < sizeMatrix; i++) {
            for (int j = 0; j < sizeMatrix; j++) {
                System.out.print(" " + matrixSubtractionResult[i][j] + " ");
            }
            System.out.println();
        }
        System.out.println();

        // Вывод матрицы
        for (int i = 0; i < sizeMatrix; i++) {
            for (int j = 0; j < sizeMatrix; j++) {
                System.out.print(" " + matrixMultiplicationResult[i][j] + " ");
            }
            System.out.println();
        }
        System.out.println();
    }

}
