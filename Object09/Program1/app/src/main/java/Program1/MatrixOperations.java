package Program1;

public class MatrixOperations {

    public int[][] matrixAddition(Matrix mat1, Matrix mat2) throws NullPointerException, MatrixException{
        int[][] matrix1 = mat1.getMatrix();
        int[][] matrix2 = mat2.getMatrix();
        int[][] matrixResult = new int[matrix1.length][matrix1.length];
        if ((matrix1 == null) || (matrix2 == null)){ throw new NullPointerException("matrix null");}
        if (matrix1.length != matrix2.length || matrix1[0].length != matrix2[0].length){ throw new MatrixException("matrix are different");}

        for (int i = 0; i < matrix1.length; i++) {
            for (int j = 0; j < matrix1.length; j++) {
                matrixResult[i][j] = matrix1[i][j] + matrix2[i][j];
            }
        }
        return matrixResult;
    }

    public int[][] matrixSubtraction(Matrix mat1, Matrix mat2) throws NullPointerException, MatrixException{
        int[][] matrix1 = mat1.getMatrix();
        int[][] matrix2 = mat2.getMatrix();
        int[][] matrixResult = new int[matrix1.length][matrix1.length];
        if ((matrix1 == null) || (matrix2 == null)){ throw new NullPointerException("matrix null");}
        if (matrix1.length != matrix2.length || matrix1[0].length != matrix2[0].length){ throw new MatrixException("matrix are different");}

        for (int i = 0; i < matrix1.length; i++) {
            for (int j = 0; j < matrix1.length; j++) {
                matrixResult[i][j] = matrix1[i][j] - matrix2[i][j];
            }
        }
        return matrixResult;
    }

    public int[][] matrixMultiplication(Matrix mat1, Matrix mat2) throws NullPointerException, MatrixException{
        int[][] matrix1 = mat1.getMatrix();
        int[][] matrix2 = mat2.getMatrix();
        int[][] matrixResult = new int[matrix1.length][matrix1.length];
        if ((matrix1 == null) || (matrix2 == null)){ throw new NullPointerException("matrix null");}
        if (matrix1[0].length != matrix2.length){ throw new MatrixException("Incorrect matrices");}

        for (int i = 0; i < matrix1.length; i++){
            for (int j = 0; j < matrix2[0].length; j++){
                for (int y = 0; y < matrix2.length; y++){
                    matrixResult[i][j] += matrix1[i][y] * matrix2[y][j];
                }
            }
        }
        return matrixResult;
    }
}
