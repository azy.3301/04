package tomcat.models;

import java.sql.SQLException;
import java.util.ArrayList;

import tomcat.utils.ModelBuilder;
import tomcat.utils.BaseConnection;

public class Holder extends BaseConnection {

    private int id;
    private String name;
    private String phone;
    private static final String[] COLUMNS = {"name", "phone"};

    private static ModelBuilder<Holder> buildModel = (rs) ->
    {
      Holder holder = null;
      try {
        holder = new Holder(rs.getString("name"), rs.getString("phone"));
      } catch (SQLException e) {
          System.out.println(e.getMessage());
      }
      return holder;
    };



    Holder(){}

    public Holder(String name, String phone) {
        this.name = name;
        this.phone = phone;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPhone() {
        return phone;
    }

    public static ArrayList<Holder> executeWithResultSet(String query) throws SQLException{
        return executeSelect(query, buildModel);
    }

    public static boolean executeWithoutResultSet(String query, String phone) throws SQLException{
        executeWithoutReturn(query);
        return getbyPhone(phone) != null;
    }



    public static ArrayList<Holder> getAll() throws SQLException{
        return executeWithResultSet(String.format("SELECT %s FROM Holder", String.join(", ", COLUMNS)));
    }

    public static Holder getbyPhone(String phone) throws SQLException{
        ArrayList<Holder> hol = executeWithResultSet(String.format("SELECT %s FROM Holder WHERE phone in ('%s')",
                                                                            String.join(", ", COLUMNS), phone));
        return hol.isEmpty() ? null : hol.get(0);
    }

    public static boolean create(String phone, String value) throws SQLException{
        return executeWithoutResultSet(String.format("INSERT INTO Holder (%s) VALUES('%s', '%s')",
                                                        String.join(", ", COLUMNS), value, phone), phone);
    }

    public static boolean deletebyPhone(String phone) throws SQLException{
        return !executeWithoutResultSet(String.format("DELETE FROM Holder WHERE %s in ('%s')",
                                                                  COLUMNS[1], phone), phone);
    }

    public static boolean update(String phone, String name) throws SQLException{
        return executeWithoutResultSet(String.format("UPDATE Holder SET %s = '%s' WHERE %s = '%s'",
                                                      COLUMNS[0], name, COLUMNS[1], phone), phone);
    }

    public String toString(){
        return String.format("%s   %s", this.name, this.phone);
    }
}
