# Task

```
Продолжение проекта из задания 1
Реализовать страницы и сервлет для создания, редактирования и удаления записей в таблице.
```

## Deployment Instructions

1. Install [tomcat-10.0.21](https://tomcat.apache.org/download-10.cgi)
2. Installnload this repository
3. gradle war
4. cp build/libs/app2.war <path-to-tomcat>/webapps
5. While in apache-tomcat-10.0.21/bin run bash catalina.sh run
6. Go to the link localhost:<port>/app2/holders