<style>
    .body{
        margin: 0;
        padding: 0;
        background-color: #FAF0DC;
        color: #000000;
    }

    .header {
        color: #E8EAE3;
        text-align: center;
        background-color: #0B4141;
        box-shadow: 0 0 15px #121212;
        top: 0;
        left: 0;
        width: 100%;
        padding: 3px;
        margin-top: 0;
        margin-bottom: 15px;
    }

    .button {
        background-color:#0B4141;
        border-color:#0B4141;
        color: #E8EAE3;
        border-radius: 2px;
    }

    .button_back{
        text-align: left;
        background-color:#0B4141;
        border-color:#0B4141;
        color: #E8EAE3;
        border-radius: 5px;
        margin-left: 15px;
        margin-bottom: 10px;
        float: left;
        margin-right: 10px;
    }

    .submit {
        background-color:#0B4141;
        border-color:#0B4141;
        color: #E8EAE3;
        border-radius: 15px;
    }

    .input {
        background-color:#0B4141;
        border-color:#0B4141;
        border-radius: 15px;
        color: #E8EAE3;
    }

    .link {
        color: #E8EAE3;
        text-align: center;
        background-color: transparent;
        text-decoration: none;
    }

    .table_line {
        float: left;
        margin-right: 10px;
        display:inline-block;
    }

    table, th, td {
        text-align: center;
        margin-left: 15px;
        margin-bottom: 15px;
        border: 2px solid black;
        border-collapse: collapse;
    }
</style>