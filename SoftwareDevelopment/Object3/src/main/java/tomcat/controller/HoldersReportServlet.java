package tomcat.controller;

import jakarta.servlet.*;
import jakarta.servlet.http.*;
import java.io.*;
import jakarta.servlet.annotation.WebServlet;
import java.util.ArrayList;

import tomcat.query_object.QueryObject;
import tomcat.dto.HolderWrapper;
import tomcat.dto.Holder;
import tomcat.dto.Equipment;


/**
  * Address servlet class "/holders_report"
  * @author AZY
  * @version %I%, %G%
  */
@WebServlet("/holders_report")
public class HoldersReportServlet extends HttpServlet
{

    /**
     * method of processing GET requests, output of the holders_report page
     * @param request  - an object containing a request received from the client
     * @param response - the object that defines the response to the client
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
                                    throws ServletException, IOException {
        ArrayList<HolderWrapper> wrappers = QueryObject.getHolderQuery(request.getParameter("phone"));
        Holder holder = new Holder(wrappers.get(0));
        ArrayList<Equipment> equipments = Equipment.getEquipments(wrappers);
        System.out.println(wrappers);
        request.setAttribute("Holder", holder);
        request.setAttribute("Equipment", equipments); 
        request.getRequestDispatcher("tomcat/views/holders_report.jsp").forward(request, response);
    }
}
