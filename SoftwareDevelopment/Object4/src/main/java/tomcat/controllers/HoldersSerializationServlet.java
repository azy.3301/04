package tomcat.controllers;

import jakarta.servlet.*;
import jakarta.servlet.http.*;
import java.io.*;
import java.net.*;
import jakarta.servlet.annotation.WebServlet;

@WebServlet("/holders_serialization")
public class HoldersSerializationServlet extends HttpServlet {

    @Override
    public void doGet(HttpServletRequest request,
                      HttpServletResponse response) throws IOException,
            ServletException {
        request.getRequestDispatcher("/tomcat/views/holders_serialization.jsp").forward(request, response);
    }
}
