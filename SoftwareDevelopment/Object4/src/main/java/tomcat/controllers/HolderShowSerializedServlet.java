package tomcat.controllers;

import jakarta.servlet.*;
import jakarta.servlet.http.*;
import java.io.*;
import java.net.*;
import jakarta.servlet.annotation.WebServlet;
import java.util.regex.Pattern;
import java.util.regex.Matcher;

import tomcat.models.Holder;
import tomcat.service.Deserialize;
import tomcat.models.SerializedHolder;
import tomcat.utils.GetFromeRequest;

@WebServlet("/holder_show_serialized/*")
public class HolderShowSerializedServlet extends HttpServlet {

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        int id = GetFromeRequest.getId(request);
        if (id != -1) {
            SerializedHolder serializedHolder = SerializedHolder.findByHolderId(id);
            if (serializedHolder != null) {
                createOutHTML(response, request, Deserialize.deserialize(serializedHolder.getData()));
            } else {
                Holder holder = Holder.findById(id);
                if (holder != null) {
                    createOutHTML(response, request, holder);
                } else {
                    response.sendError(HttpServletResponse.SC_NOT_FOUND);
                }
            }
        } else {
            response.sendError(HttpServletResponse.SC_NOT_FOUND);
        }
    }

    private static void createOutHTML(HttpServletResponse response, HttpServletRequest request, Holder holder) throws IOException, ServletException {
        request.setAttribute("holder", holder);
        request.getRequestDispatcher("/tomcat/views/holder_show_serialized.jsp").forward(request, response);
    }
}
