package tomcat.utils;


import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.sql.PreparedStatement;
import java.sql.Statement;

import tomcat.configs.ConnectionDB;

public class BaseConnection {

    protected static <T> ArrayList<T> executeSelect(String query, ModelBuilder<T> model){
        ArrayList<T> newObject = new ArrayList<T>();
        try {
            Statement statement = ConnectionDB.getConnection().createStatement();
            ResultSet resultSet = statement.executeQuery(query);

            while (resultSet.next()) {
                newObject.add(model.build(resultSet));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return newObject;
    }

    public static PreparedStatement getPreparedStatement(String query) {
        PreparedStatement stmt = null;
        try {
            stmt = ConnectionDB.getConnection().prepareStatement(query);
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return stmt;
    }

    public static void executeWithOutRs(String query) {
        try (Statement stmt = ConnectionDB.getConnection().createStatement()) {
            stmt.executeUpdate(query);
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    public static PreparedStatement getPreparedStatement(String query, String[] generatedColumns) {
        PreparedStatement stmt = null;
        try {
            stmt = ConnectionDB.getConnection().prepareStatement(query, generatedColumns);
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return stmt;
    }

    protected static <T> T executeSelectId(String query, ModelBuilder<T> model){
        return executeSelect(query, model).get(0);
    }


    protected static void execute(String query){
        try {
            Statement statement = ConnectionDB.getConnection().createStatement();
            statement.execute(query);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    protected static void executeAdd(String query){
        execute(query);
    }

    protected static void executeUpdate(String query){
        execute(query);
    }

    protected static void executeDeleted(String query){
        execute(query);
    }

    public static void executeWithoutReturn(String query){
        execute(query);
    }
}