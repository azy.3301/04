package tomcat.utils;

import java.sql.ResultSet;

public interface ModelBuilder<T> {
     T build(ResultSet rs);
}