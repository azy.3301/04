package tomcat.service;

import java.util.Base64;
import java.io.ObjectInputStream;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.Serializable;

import tomcat.models.Holder;

public class Deserialize {
    public static Holder deserialize(String serializedObject) throws IOException {
        byte[] data = Base64.getDecoder().decode(serializedObject);
        ObjectInputStream ois = new ObjectInputStream(new ByteArrayInputStream(data));
        Holder holder = null;
        try {
            holder = (Holder) ois.readObject();
        } catch (ClassNotFoundException e) {
        }
        ois.close();
        return holder;
    }
}
