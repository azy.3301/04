package tomcat.models;

import java.io.Serializable;
import java.sql.SQLException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

import tomcat.utils.BaseConnection;
import tomcat.utils.ModelBuilder;

public class Holder extends BaseConnection implements Serializable {

    private String name;

    private String phone;

    private int id;

    private static final ModelBuilder<Holder> modelExtractor = (rs) -> {
        Holder holder = null;
        try {
            holder = new Holder(
                    rs.getString("name"),
                    rs.getString("phone"),
                    rs.getInt("id"));
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return holder;
    };

    private static final String[] COLUMN_NAMES = new String[]{"name", "phone"};

    private static final String TABLE_NAME = "Holder";

    Holder(String name, String phone, int id) {
        this.name = name;
        this.phone = phone;
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public String getPhone() {
        return phone;
    }

    public int getId() {
        return id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public static int create(String name, String phone) {
        PreparedStatement stmtCreate = getPreparedStatement(
                String.format("INSERT INTO %s (%s) VALUES(?, ?)", TABLE_NAME, String.join(", ", COLUMN_NAMES)),
                new String[]{"id"}
        );
        int newId = -1;
        try {
            stmtCreate.setString(1, name);
            stmtCreate.setString(2, phone);
            stmtCreate.executeUpdate();
            ResultSet gk = stmtCreate.getGeneratedKeys();
            if (gk.next()) {
                newId = gk.getInt("id");
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return newId;
    }

    public boolean save() {
        Holder holder = findById(id);
        if (holder != null) {
            PreparedStatement stmtUpdate = getPreparedStatement(
                    String.format("UPDATE %s SET %s = ? WHERE id = ?", TABLE_NAME, String.join(" = ?, ", COLUMN_NAMES)));
            try {
                stmtUpdate.clearParameters();
                stmtUpdate.setString(1, name);
                stmtUpdate.setString(2, phone);
                stmtUpdate.setInt(3, id);
                stmtUpdate.executeUpdate();
            } catch (SQLException e) {
                System.out.println(e.getMessage());
            }
        } else {
            id = create(name, phone);
            holder = findById(id);
        }
        return holder != null && holder.getName().equals(name) && holder.getPhone().equals(phone);
    }

    public boolean delete() {
        executeWithOutRs(String.format("DELETE FROM %s WHERE id = %s", TABLE_NAME, id));
        return findById(id) == null;
    }

    public static Holder[] all() {
        return executeSelect("SELECT * FROM " + TABLE_NAME);
    }

    public static Holder findByPhone(String phone) {
        return executeSelectOneInstance(String.format("SELECT %s, id FROM %s WHERE phone = '%s'", String.join(", ", COLUMN_NAMES), TABLE_NAME, phone));
    }

    public static Holder findById(int id) {
        return executeSelectOneInstance(String.format("SELECT %s, id FROM %s WHERE id = %s", String.join(", ", COLUMN_NAMES), TABLE_NAME, id));
    }

    private static Holder executeSelectOneInstance(String query) {
        Holder[] holderInList = executeSelect(query);
        return (holderInList.length > 0) ? holderInList[0] : null;
    }

    private static Holder[] executeSelect(String query) {
        ArrayList<Holder> holders = executeSelect(query, modelExtractor);
        return holders.toArray(new Holder[holders.size()]);
    }

    @Override
    public String toString() {
        return String.format("id: %s; name: %s; phone %s", id, name, phone);
    }
}
