<%@ page contentType="text/html; charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
    <style>
    .body{
        margin: 0;
        padding: 0;
        padding-left: 15px;
        background-color: #FAF0DC;
        color: #000000;
    }

    .button {
        background-color:#0B4141;
        border-color:#0B4141;
        color: #E8EAE3;
        border-radius: 2px;
    }
    </style>
    <body class="body">
        <h1>Holder</h1>
        <p><%=request.getAttribute("message")%></p>
        <form method="get" action="/app4/holders_serialization">
            <button class="button" type="submit">exit</button>
        </form>
    </body>
</html>
