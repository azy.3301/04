<%@ page contentType="text/html; charset=UTF-8" language="java" %>
<%@ page import="tomcat.models.Holder" %>

<!DOCTYPE html>
<html>
    <style>
        .body{
            margin: 0;
            padding: 0;
            padding-left: 10px;
            background-color: #FAF0DC;
            color: #000000;
        }
    
        .button {
            background-color:#0B4141;
            border-color:#0B4141;
            color: #E8EAE3;
            border-radius: 2px;
        }
    
        table, th, td {
            text-align: center;
            margin-left: 15px;
            border: 2px solid black;
            border-collapse: collapse;
        }
    </style>
    <body class="body">
    <h1>Holders</h1>
    <table border:1 rules="rows">
        <tr>
            <th>ID</th>
            <th>Name</th>
            <th>Phone</th>
            <th>show</th>
            <th>serialize</th>
        </tr>
        <%for (Holder holder : Holder.all()) {%>
        <tr>
            <td><%= holder.getId() %>
            </td>
            <td><%= holder.getName() %>
            </td>
            <td><%= holder.getPhone() %>
            </td>
            <td>
                <form method="get" action="/app4/holder_show_serialized/<%= holder.getId() %>">
                    <button class="button" type="submit">show</button>
                </form>
            </td>
            <td>
                <form method="post" action="/app4/holder_serialize/<%= holder.getId() %>">
                    <button class="button" type="submit">serialize</button>
                </form>
            </td>
        </tr>
        <%}%>
    </table>
    </body>
</html>