package Program1;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

class NumberСonfirmationTest {
    @Test void confirmation() {
        assertTrue(NumberСonfirmation.confirmation("79996168899"));
        assertTrue(NumberСonfirmation.confirmation("+79996168899"));
        assertTrue(NumberСonfirmation.confirmation("+7(999)6168899"));
        assertTrue(NumberСonfirmation.confirmation("+7 (999)6168899"));
        assertTrue(NumberСonfirmation.confirmation("+7 (999) 616 88 99"));
        assertTrue(NumberСonfirmation.confirmation("+7(999)-616-88-99"));
        assertTrue(NumberСonfirmation.confirmation("89996168899"));
        assertTrue(NumberСonfirmation.confirmation("9996168899"));
        assertFalse(NumberСonfirmation.confirmation("+7 999 616 898 99"));
        assertFalse(NumberСonfirmation.confirmation("A996168899"));
        assertFalse(NumberСonfirmation.confirmation("+Se(999)-616-88-99"));
        assertFalse(NumberСonfirmation.confirmation("999"));
        assertFalse(NumberСonfirmation.confirmation(""));
    }
}
