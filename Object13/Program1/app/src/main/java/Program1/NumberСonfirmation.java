package Program1;

import java.util.regex.Pattern;
import java.util.regex.Matcher;

/**
  * This is NumberСonfirmation.
  * Here we check if the user entered the number correctly.
  * @author AZY
  * @version %I%, %G%
  */
public class NumberСonfirmation {
    
    /**
    * The method checks the correctness of the phone number.
    * @param phoneNumber Number to check.
    * @return true or false.
    */
    public static boolean confirmation(String phoneNumber){
        String regex = "\\d{10}";
        phoneNumber = phoneNumber.replaceAll("\\s|-|\\(|\\)|^(8|7|\\+7)","");
        System.out.println(phoneNumber);
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(phoneNumber);
        return matcher.matches();
    }
}
