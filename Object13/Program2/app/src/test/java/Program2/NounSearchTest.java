package Program2;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

class NounSearchTest {
    @Test void correctAnswer() {
        String str = "В пятой палате сейчас кварцевание.";
        assertEquals(NounSearch.nounSearch(str), "палате ");
    }

    @Test void notCorrectAnswer() {
        String str = "У нас было пять лекции за весь семестр.";
        assertNotEquals(NounSearch.nounSearch(str), "");
    }

    @Test void noLine() {
        String str = "";
        assertEquals(NounSearch.nounSearch(str), "");
    }
}

