package Program2;

import java.util.regex.Pattern;
import java.util.regex.Matcher;

/**
  * This is NounSearch.
  * Here we are checking if there is a forward noun "5".
  * @author AZY
  * @version %I%, %G%
  */
public class NounSearch {
    
    /**
    * The method checks if there is a noun before "five".
    * @param str str to check.
    * @return true or false.
    */
    public static String nounSearch(String str){
        String regex = "(П|п)ят(ь|ый|ая|ое|ые|ого|ому|ом|ой|ую|ою|ых|ым|ыми)\\s(\\w+)";
        Pattern pattern = Pattern.compile(regex, Pattern.UNICODE_CHARACTER_CLASS);
        Matcher matcher = pattern.matcher(str);

        String newStr = "";
        while(matcher.find()) {
            newStr += matcher.group(3) + " ";
        }

        return newStr;
    }
}
