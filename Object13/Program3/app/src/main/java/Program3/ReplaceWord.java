package Program3;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

 /**
  * 
  * This is replaceWord.
  * Here we are replacing words.
  * @author AZY
  * @version %I%, %G%
  */
public class ReplaceWord {
    
    /**
    * The method checks if there is a noun before "five".
    * @param str str to check.
    * @return new string.
    */
    public static String replaceWord(String str, String[] unlovedWord, String lovedWorld) {
        String unlovedWordStr = "\\b(" +String.join("|", unlovedWord) + ")\\b";
        System.out.println(unlovedWordStr);
        return Pattern.compile(unlovedWordStr)
                    .matcher(str)
                    .replaceAll(lovedWorld);
    }
}
