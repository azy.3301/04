package Program3;

public class Main {
    public static void main(String[] args) {
        String str = "ехать медленно";
        String[] unlovedWord = new String[]{"медленно", "просто"};
        String lovedWord = "быстро";
        System.out.println(ReplaceWord.replaceWord(str, unlovedWord, lovedWord));

        String str2 = "распятый монстр";
        String[] unlovedWord2 = new String[]{"пятый"};
        String lovedWord2 = "шестой";
        System.out.println(ReplaceWord.replaceWord(str2, unlovedWord2, lovedWord2));
    }
}