package Object15;

import java.io.Serial;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Random;
import org.apache.commons.lang3.RandomStringUtils;

/**
  * This is Student.
  * @author AZY
  * @version 1
  */
public class Student implements Serializable {
    
    /** id student */
    private final int id;
    
    /** name student */
    private final String name;

    /** age student */
    private final int age;

    /** control version */
    @Serial
    private static final long serialVersionUID = -5554443332223333322L;

    /**
     * Using this method, you can get the student ID.
     * @param id id
     * @param name name
     * @param age age
      */
    public Student(int id, String name, int age){
        this.id = id;
        this.name = name;
        this.age = age;
    }

    /**
      * Using this method, you can get the student ID.
      * @return Student id
      */
    public int getId(){
        return this.id;
    }

    /**
      * Using this method, you can get the student name.
      * @return Student name
      */
    public String getName(){
        return this.name;
    }

    /**
      * Using this method, you can get the student age.
      * @return Student name
      */
    public int getAge(){
        return this.age;
    }

     /**
      * overridden method toString().
      * @return String
      */
      public String toString(){
        return ("id : " + this.id + "\nname : " + this.name + "\nage : " + this.age);
    }

    /**
     * generates a random Map.
     * @param num - num.
     * @return generated Map.
     */
      public static HashMap<Integer, Student> generateMapStudent(int num){
        HashMap<Integer, Student> myMap = new HashMap<Integer, Student>();
        for (int i = 0; i < num; i++) {
            myMap.put(i + 1, new Student(i + 1, RandomStringUtils.randomAlphabetic(10), (int) (Math.random() * 10)));
        }
        return myMap;
    }
}
