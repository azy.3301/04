package Object15;

import java.io.*;
import java.util.HashMap;
import java.util.Map;

/**
  * This is Student.
  * @author AZY
  * @version 1
  */
public class Serialize {

    /**
     * This is serializeStudent
     * @param pathFile path file
     * @param mapStudent map student
     */
    public static void serializeStudent(String pathFile, Map<Integer, Student> mapStudent){
        try {
            FileOutputStream fos = new FileOutputStream(pathFile) ;
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(mapStudent) ;
            fos.close();
            } catch (IOException e) {
                e.printStackTrace();
        }
    }

    /**
     * This is deserializeStudent
     * @param pathFile path file
     */
    public static void deserializeStudent(String pathFile){
        try {
            FileInputStream fis = new FileInputStream(pathFile) ;
            ObjectInputStream ois = new ObjectInputStream(fis);
            var studentMap = (HashMap<Integer, Student>) ois.readObject();

            for (HashMap.Entry<Integer, Student> student : studentMap.entrySet()) {
                System.out.println(student.getValue());
            }

            ois.close();
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
}
