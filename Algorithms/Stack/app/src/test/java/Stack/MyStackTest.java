package Stack;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class MyStackTest {
    
    MyStack<Integer> stack = new MyStack<>(10);


    @Test void push() {
        stack.push(3);
        assertEquals(3, stack.peek());
    }

    @Test void pop() {
        stack.push(3);
        stack.push(2);
        stack.push(4);
        stack.pop();
        assertEquals(2, stack.peek());
    }

    @Test void full() {
        assertFalse(stack.full());
    }

    @Test void empty() {
        assertTrue(stack.empty());
    }

    @Test void size() {
        stack.push(2);
        assertEquals(1, stack.size());
    }
    
    @Test void isEmpty() {
        assertTrue(stack.isEmpty());
    }

    @Test
    void contains() {
        stack.push(2);
        assertTrue(stack.contains(2));
        assertFalse(stack.contains(4));
    }

    @Test
    void iterator() {
        stack.add(3);
        Iterator<Integer> iterator = stack.iterator();
        assertTrue(iterator.hasNext());
        iterator.next();
        assertFalse(iterator.hasNext());
    }

    @Test
    void remove() {
        stack.push(2);
        stack.push(4);
        assertEquals(4, stack.remove());
    }

    @Test
    void add() {
        stack.add(3);
        assertEquals(3, stack.peek());
    }

    @Test
    void removeObject() {
        stack.add(1);
        stack.add(2);
        assertEquals(2, stack.remove());
    }

    List<Integer> list = new ArrayList<>();
    int i = 2;
    int x = 5;

    @Test
    void containsAll() {
        list.add(i);
        list.add(x);
        stack.add(i);
        stack.add(x);
        assertTrue(stack.containsAll(list));
    }


    @Test
    void addAll() {
        list.add(i);
        list.add(x);
        stack.addAll(list);
        assertEquals(5, stack.peek());
    }

    @Test
    void removeAll() {
        list.add(i);
        list.add(x);
        stack.push(2);
        stack.push(3);
        stack.push(5);
        stack.removeAll(list);
        assertEquals(3, stack.peek());
    }

    @Test
    void retainAll() {
        list.add(i);
        list.add(x);
        stack.push(3);
        stack.push(5);
        stack.push(2);
        stack.retainAll(list);
        assertEquals(2, stack.peek());
    }

    @Test
    void clear() {
        stack.add(1);
        stack.add(2);
        stack.clear();
        stack.add(1);
        assertEquals(1, stack.peek());
    }
}