## Task

```
Создать отображение размером в 2 000 000 записей.
Ключ - строка из 10 случайных символов.
Значение - случайное число.
Вывести значения отсортированные по возрастанию.
Сравнить скорость работы для разных базовых классов.
```

## Links

[UML](https://app.diagrams.net/#G1_ibrQamc51bN137xxNt0WoKgzSXH83s8)

[google sheets](https://docs.google.com/spreadsheets/d/157Ij-Ga6Rg3TA4QLUDk_dz9PdneVtuYBSsD1BR3xtuc/edit?usp=sharing)

## Benchmark  

```
Benchmark                                          Mode  Cnt    Score   Error  Units
Program1.SampleBenchmark.generationHashMap        thrpt    2    0.959          ops/s
Program1.SampleBenchmark.generationLinkedHashMap  thrpt    2    1.008          ops/s
Program1.SampleBenchmark.generationTreeMap        thrpt    2    0.509          ops/s
Program1.SampleBenchmark.outputList               thrpt    2    0.081          ops/s
Program1.SampleBenchmark.sortList                 thrpt    2  393.292          ops/s
```
