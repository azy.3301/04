package Program1;

import java.util.HashMap;
import java.util.Hashtable;
import java.util.LinkedHashMap;
import java.util.TreeMap;
import java.util.Map;
import java.util.Set;
import java.util.ArrayList;
import java.util.Collections;

/**
  * This is TestMap.
  * Here we measure the time to generate, sort, and render the Map.
  * @author AZY
  * @version %I%, %G%
  */
public class TestTime {

    /** Constant value for generating random numbers */
    final int ITEMS = 2000000;
    /** ArrayList for sortList method */
    Map<String, Integer> map;
    /** ArrayList for outputList method */
    ArrayList<Integer> sortList;
    
    /**
    * Constructor
    */
    public TestTime(){
        HashMap<String, Integer> hashMap = new HashMap();
        this.map = TestMap.generateMap(hashMap, this.ITEMS);
        this.sortList = TestMap.sortList(this.map);
    }

     /**
    * Method for calculating HashMap generation time.
    * @return received time
    */
    public long generateHashMap() {  
        HashMap<String, Integer> hashMap = new HashMap();
        long startTime = System.currentTimeMillis();
        TestMap.generateMap(hashMap, this.ITEMS);
        return System.currentTimeMillis() - startTime;
    }

    /**
    * Method for calculating Hashtable generation time.
    * @return received time
    */
    public long generateHashtable() {  
        Hashtable<String, Integer> hashTable = new Hashtable();
        long startTime = System.currentTimeMillis();
        TestMap.generateMap(hashTable, this.ITEMS);
        return System.currentTimeMillis() - startTime;
    }

    /**
    * Method for calculating LinkedHashMap generation time.
    * @return received time
    */
    public long generateLinkedHashMap() {
        LinkedHashMap<String, Integer> linkedHashMap = new LinkedHashMap();
        long startTime = System.currentTimeMillis();
        TestMap.generateMap(linkedHashMap, this.ITEMS);
        return System.currentTimeMillis() - startTime;
    }

    /**
    * Method for calculating TreeMap generation time.
    * @return received time
    */ 
    public long generateTreeMap() {
        TreeMap<String, Integer> treeMap = new TreeMap();
        long startTime = System.currentTimeMillis();
        TestMap.generateMap(treeMap, this.ITEMS);
        return System.currentTimeMillis() - startTime;
    }

    /**
    * Method for calculating sorting time of ArrayList.
    * @return received time
    */
    public long sortList(){  
        long startTime = System.currentTimeMillis();
        TestMap.sortList(this.map);
        return System.currentTimeMillis() - startTime;
    }
    
    /**
    * Method for counting the output time of ArrayList.
    * @return received time
    */
    public long outputList(){  
        long startTime = System.currentTimeMillis();
        TestMap.outputList(this.sortList);
        return System.currentTimeMillis() - startTime;
    }
}
