package Program1;

public class Main {

    public static void main(String[] args) throws Exception {
        
        TestTime testTime = new TestTime();

        System.out.println("outputList = " + testTime.outputList());
        System.out.println("generationHashMap = " + testTime.generateHashMap());
        System.out.println("generationHashtable = " + testTime.generateHashtable());
        System.out.println("generationLinkedHashMap = " + testTime.generateLinkedHashMap());
        System.out.println("generationTreeMap = " + testTime.generateTreeMap());
        System.out.println("sortList = " + testTime.sortList());
    }
}
