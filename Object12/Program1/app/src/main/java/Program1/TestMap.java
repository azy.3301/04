package Program1;

import java.util.HashMap;
import java.util.Hashtable;
import java.util.LinkedHashMap;
import java.util.TreeMap;
import java.util.Map;
import java.util.Set;
import java.util.ArrayList;
import java.util.Collections;

import java.util.Random;
import org.apache.commons.lang3.RandomStringUtils;

/**
  * This is TestMap.
  * Here we are generating, sorting and outputting a Map.
  * @author AZY
  * @version %I%, %G%
  */
public class TestMap {

    /**
    * Method for populating a Map with data.
    * @param map - Map to fill.
    * @param items - how many elements to add to Map
    * @return generated Map
    */
    public static Map<String, Integer>  generateMap(Map map, int items){
        for (int i = 1; i <= items; i++){
            String generatedString = RandomStringUtils.randomAlphabetic(10);
            int generatedInt = (int) (Math.random() * 10);
            map.put(generatedString, generatedInt);
        }
        return map;
    }

    /**
    * Method for soting ArrayList
    * @param map - get valuse and sorted.
    * @return sorted ArrayList
    */
    public static ArrayList<Integer> sortList(Map<String, Integer> map){     
        ArrayList<Integer> list = new ArrayList<Integer>(map.values());
        Collections.sort(list);
        return list;
    }

    /**
    * Method for outputting ArrayList
    * @param list - ArrayList to be output.
    */
    public static void outputList(ArrayList<Integer> list){
        int index = 1;
        for (Integer item : list){
            System.out.println(index + ": " + item);
            index++;
        }
    }
}
