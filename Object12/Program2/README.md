## Task

```
Создать отображение размером в 2 000 000 записей.
Ключ - строка из 10 случайных символов.
Значение - случайное число.
Создать массив из 30 000 строк длиной 10 случайных символов. Найти, сумму значений отображения, соответствующих строкам из массива. Запрещено создавать дополнительные структуры данных.
Сравнить скорость работы для разных базовых классов.
```

## Links

[UML](https://app.diagrams.net/#G1_ibrQamc51bN137xxNt0WoKgzSXH83s8)

[google sheets](https://docs.google.com/spreadsheets/d/157Ij-Ga6Rg3TA4QLUDk_dz9PdneVtuYBSsD1BR3xtuc/edit?usp=sharing)

## Benchmark  

```
Benchmark                                          Mode  Cnt     Score   Error  Units
Program2.SampleBenchmark.generationArray          thrpt    2   136.515          ops/s
Program2.SampleBenchmark.generationHashMap        thrpt    2     0.973          ops/s
Program2.SampleBenchmark.generationHashtable      thrpt    2     0.952          ops/s
Program2.SampleBenchmark.generationLinkedHashMap  thrpt    2     0.909          ops/s
Program2.SampleBenchmark.generationTreeMap        thrpt    2     0.464          ops/s
Program2.SampleBenchmark.sumMapValues             thrpt    2  1707.840          ops/s
```
