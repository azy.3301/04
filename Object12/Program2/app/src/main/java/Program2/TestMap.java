package Program2;

import java.util.HashMap;
import java.util.Hashtable;
import java.util.LinkedHashMap;
import java.util.TreeMap;
import java.util.Map;
import java.util.Set;
import java.util.ArrayList;
import java.util.Collections;

import java.util.Random;
import org.apache.commons.lang3.RandomStringUtils;

/**
  * This is TestMap.
  * Here we are generating, sorting and outputting a Map.
  * @author AZY
  * @version %I%, %G%
  */
public class TestMap {

    /**
    * Method for filling the Map with data.
    * @param map - Map to fill.
    * @param items - how many elements to add to Map
    * @return generated Map
    */
    public static Map<String, Integer>  generateMap(Map map, int items){
        for (int i = 1; i <= items; i++){
            String generatedString = RandomStringUtils.randomAlphabetic(10);
            int generatedInt = (int) (Math.random() * 10);
            map.put(generatedString, generatedInt);
        }
        return map;
    }

    /**
     * Method for filling the array with data.
     * @param sizeArray - generated array size
     * @return generated Array
     */
    public static String[] generateArray(int sizeArray){
        String[] array = new String[sizeArray];
        for (int i = 0; i < array.length; i++){
            array[i] = RandomStringUtils.randomAlphabetic(10);
        }
        return array;
    }

    /**
     * method adds Map values by keys from array
     * @param map - mup
     * @param array - search for values by which to sort
     * @return sum Map Values
     */
    public static int sumMapValues(Map<String, Integer> map, String[] array){
        int sum = 0;
        for (String str : array) {
            Integer values = map.get(str);
            if (values != null){
                sum += map.get(str);
            }
        }
        return sum;
    }
}
