package Program2;

import java.util.HashMap;
import java.util.Hashtable;
import java.util.LinkedHashMap;
import java.util.TreeMap;

public class Main {

    public static void main(String[] args) throws Exception {
        
        TestTime testTime = new TestTime();

        HashMap<String, Integer> hashMap = new HashMap();
        System.out.println("generationHashMap = " + testTime.generateMap(hashMap));
        Hashtable<String, Integer> hashTable = new Hashtable();
        System.out.println("generationHashtable = " + testTime.generateMap(hashTable));
        LinkedHashMap<String, Integer> linkedHashMap = new LinkedHashMap();
        System.out.println("generationLinkedHashMap = " + testTime.generateMap(linkedHashMap));
        TreeMap<String, Integer> treeMap = new TreeMap();
        System.out.println("generationTreeMap = " + testTime.generateMap(treeMap));
        System.out.println("generationArray = " + testTime.generateArray());
        System.out.println("sumMapValues = " + testTime.sumMapValues());
    }
}
