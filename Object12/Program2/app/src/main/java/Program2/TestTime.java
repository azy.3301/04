package Program2;

import java.util.HashMap;
import java.util.Hashtable;
import java.util.LinkedHashMap;
import java.util.TreeMap;
import java.util.Map;
import java.util.Set;
import java.util.ArrayList;
import java.util.Collections;

/**
  * This is TestMap.
  * Here we measure the time to generate, sort, and render the Map.
  * @author AZY
  * @version %I%, %G%
  */
public class TestTime {

    /** Constant value for generating random numbers */
    final int ITEMS = 2_000_000;
    /** Constant value SIZE_ARRAY */
    final int SIZE_ARRAY = 30_000;    

    public long generateMap(Map map) {  
        long startTime = System.currentTimeMillis();
        TestMap.generateMap(map, this.ITEMS);
        return System.currentTimeMillis() - startTime;
    }

    /**
   // *Method for calculating Array generation time.
   // * @return received time
   // */
   public long generateArray(){
       long startTime = System.currentTimeMillis();
       TestMap.generateArray(this.SIZE_ARRAY);
       return System.currentTimeMillis() - startTime;
   }

    /**
    // *Method for calculating lookup times and values from Map.
    // * @return received time
    // */
    public long sumMapValues(){
        HashMap<String, Integer> hashMap = new HashMap();
        Map<String, Integer> map = TestMap.generateMap(hashMap, this.ITEMS); 
        String[] array = TestMap.generateArray(this.SIZE_ARRAY);
        long startTime = System.currentTimeMillis();
        TestMap.sumMapValues(map, array);
        return System.currentTimeMillis() - startTime;
    }
}
