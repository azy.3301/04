package Program2;

import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.Scope;
import org.openjdk.jmh.annotations.State;
import org.openjdk.jmh.annotations.Setup;
import org.openjdk.jmh.infra.Blackhole;
import org.openjdk.jmh.annotations.Level;

import java.util.Map;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.LinkedHashMap;
import java.util.TreeMap;
import java.util.ArrayList;

@State(Scope.Benchmark)
public class SampleBenchmark {
    
    final int ITEMS = 2_000_000;
    final int SIZE_ARRAY = 30_000;
    Map<String, Integer> hashMap;
    String[] array;

    public SampleBenchmark(){
        HashMap<String, Integer> hashMap = new HashMap();
        this.hashMap = TestMap.generateMap(hashMap, this.ITEMS);
        this.array = TestMap.generateArray(this.SIZE_ARRAY);
    }

    @Setup(Level.Trial) @Benchmark 
    public void generateHashMap() {    
        HashMap<String, Integer> hashMap = new HashMap();
        TestMap.generateMap(hashMap, this.ITEMS);
    } 

    @Setup(Level.Trial) @Benchmark 
    public void generateHashtable() {    
        Hashtable<String, Integer> HashTable = new Hashtable();
        TestMap.generateMap(HashTable, this.ITEMS);
    }

    @Setup(Level.Trial) @Benchmark
    public void generateLinkedHashMap() {
        LinkedHashMap<String, Integer> linkedHashMap = new LinkedHashMap();
        TestMap.generateMap(linkedHashMap, this.ITEMS);
    }

    @Setup(Level.Trial) @Benchmark
    public void generateTreeMap() {
        TreeMap<String, Integer> treeMap = new TreeMap();
        TestMap.generateMap(treeMap, this.ITEMS);
    }

    @Setup(Level.Trial) @Benchmark
    public void generateArray() {
        TestMap.generateArray(this.SIZE_ARRAY);
    }

    @Setup(Level.Trial) @Benchmark
    public void sumMapValues() {
        TestMap.sumMapValues(this.hashMap, this.array);
    }
}
