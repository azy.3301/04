// У армии есть набор ракет - стратегические (дальность от 10 тыс. до 40 тыс. км), оперативные (от 500 км до 10 тыс. км) и тактические (до 500 км). Длина возможного полета ракеты - случайная величина внутри соответствующего интервала. Сколько ракет из арсенала может долететь до цели, которая находится на заданном расстоянии?
package src.ru;

public class Main {
    public static void main(String[] args) {

        Rocket[] rocket = new Rocket[4];

        rocket[0] = new Strategic();
        rocket[1] = new Strategic();
        rocket[2] = new Operational();
        rocket[3] = new Tactical();

        int target = 110000;
        int hits = 0;

        for (int i = 0; i < rocket.length; i++) {
            if (rocket[i].getDistance() > target) {
                hits += 1;
            }
        }
        System.out.println(hits);
    }
}
