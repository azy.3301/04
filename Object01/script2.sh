#!/bin/bash

START=$(date +%s%N)

let "z=2**17"
c=0
while [ $c -lt $z ]; do
	let "o=2**$c"
	printf "$o \n" >> 1.txt
	c=$(($c+1))
done

END=$(date +%s%N)
DIFF=$((($END-$START)/1000000000))
echo "$DIFF"
