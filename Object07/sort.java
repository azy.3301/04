public class sort{
    public static void main(String[] args) {
        int [] array = new int[] {64, 42, 73, 41, 32, 53, 16, 24, 57, 42, 74, 55, 36};
        Sort(array, 0, array.length - 1);
        System.out.println(arrayToString(array));
    }

    public static void Sort(int[] arr, int from, int to) {
        if (from < to) {
            int divideIndex = partition(arr, from, to);
            Sort(arr, from, divideIndex - 1);
            Sort(arr, divideIndex, to);
        }
    }

    private static int partition(int[] arr, int from, int to) {
        int rightIndex = to;
        int leftIndex = from;
        int element = arr[from + (to - from) / 2];
        while (leftIndex <= rightIndex) {
            while (arr[leftIndex] < element) {
                leftIndex++;
            }
            while (arr[rightIndex] > element) {
                rightIndex--;
            }
            if (leftIndex <= rightIndex) {
                swap(arr, rightIndex, leftIndex);
                leftIndex++;
                rightIndex--;
            }
        }
        return leftIndex;
    }

    private static void swap(int[] array, int index1, int index2) {
        int tmp  = array[index1];
        array[index1] = array[index2];
        array[index2] = tmp;
    }

    private static String arrayToString(int[] array) {
        String sb = new String();
        for (int i = 0; i < array.length; i++) {
            sb = sb + array[i] +  " "; //спросить про ".append"  - sb.append(array[i] +  " ");
        }
        return sb.toString();
    }
}
